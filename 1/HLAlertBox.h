//
//  HLAlertBox.h
//  HL
//
//  Created by HerryLee on 15/7/6.
//  Copyright © 2015年 HerryLee. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
/**
 *  alert样式
 */
typedef NS_ENUM(NSInteger, HLAlertBoxStyle) {
    /**
     *  弹窗
     */
    HLAlertBoxStyleAlertView,
    /**
     *  actionSheet
     */
    HLAlertBoxStyleActionSheet
};
/**
 *  操作类型
 */
typedef NS_ENUM(NSInteger, HLAlertBoxActionType) {
    /**
     *  默认
     */
    HLAlertBoxActionTypeDfault,
    /**
     *  取消
     */
    HLAlertBoxActionTypeCancel,
    /**
     *  销毁（红色字体）
     */
    HLAlertBoxActionTypeDestructive
};
typedef void(^HLAlertBoxButtonAction)(void);

@interface HLAlertBox : NSObject
/**
 *
 *  @param viewController UIAlertController prsent的载体
 *  @param style          弹窗/actionSheet
 *  @param title          标题
 *  @param message        描述
 *
 */
- (instancetype)initWithDelegate:(UIViewController *)viewController
                           style:(HLAlertBoxStyle)style
                           title:(NSString *)title
                         message:(NSString *)message;
/**
 *  添加操作
 *
 *  @param title   按钮标题
 *  @param style   操作类型
 *  @param handler block
 */
- (void)addButtonWithTitle:(NSString *)title alertStyle:(HLAlertBoxActionType)style buttonAction:(HLAlertBoxButtonAction)handler;

/**
 *  添加TextField，当SJBAlertBoxStyleActionSheet时该方法doNothing
 *  iOS8以下最多只能添加2个TextField,且添加2个输入框后自动变成UIAlertViewStyleLoginAndPasswordInput样式
 *  @param configurationHandler handler
 */
- (void)addTextFieldWithConfigurationHandler:(void (^__nullable)(UITextField *textField))configurationHandler;
//iOS8以下最多只有2个TextField，iOS8以上无限制，当SJBAlertBoxStyleActionSheet时该该属性为nil
@property (nullable, nonatomic, readonly) NSArray <UITextField *> *textFields;

- (void)show;
@end
NS_ASSUME_NONNULL_END
