//
//  HLAlertBox.m
//  HL
//
//  Created by HerryLee on 15/7/6.
//  Copyright © 2015年 HerryLee. All rights reserved.
//

#import "HLAlertBox.h"

static NSMutableArray *hl_alert_queue;

static inline BOOL alertControllerAvailable() {
    return NSClassFromString(@"UIAlertController") != nil;
}

@interface HLAlertBox () <UIAlertViewDelegate, UIActionSheetDelegate>
@property (nonatomic, assign) HLAlertBoxStyle alertStyle;
@property (nonatomic, copy) NSArray *textFieldArray;
@end

@implementation HLAlertBox {
    id alertView;
    UIViewController *__weak delegate;
    NSMutableDictionary *actionButtonCallBacks;
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
- (instancetype)initWithDelegate:(UIViewController *)viewController
                           style:(HLAlertBoxStyle)style
                           title:(NSString *)title
                         message:(NSString *)message {
    if (self = [super init]) {
        _alertStyle = style;
        delegate = viewController;
        
        if (!hl_alert_queue) {
            hl_alert_queue = [NSMutableArray array];
        }
        
        if (alertControllerAvailable()) {
            alertView = (UIAlertController *)
            [UIAlertController alertControllerWithTitle:title
                                                message:message
                                         preferredStyle:_alertStyle == HLAlertBoxStyleAlertView ? UIAlertControllerStyleAlert : UIAlertControllerStyleActionSheet];
        } else {
            actionButtonCallBacks = [NSMutableDictionary dictionary];
            
            if (_alertStyle == HLAlertBoxStyleAlertView) {
                alertView = (UIAlertView *)[[UIAlertView alloc] initWithTitle:title
                                                                      message:message
                                                                     delegate:nil
                                                            cancelButtonTitle:nil
                                                            otherButtonTitles:nil, nil];
                ((UIAlertView *)alertView).delegate = self;
                ((UIAlertView *)alertView).alertViewStyle = UIAlertViewStyleDefault;
            } else {
                alertView = (UIActionSheet *)[[UIActionSheet alloc] initWithTitle:title
                                                                         delegate:nil
                                                                cancelButtonTitle:nil
                                                           destructiveButtonTitle:nil
                                                                otherButtonTitles:nil, nil];
                ((UIActionSheet *)alertView).delegate = self;
                ((UIActionSheet *)alertView).actionSheetStyle = UIActionSheetStyleDefault;
            }
        }
        [hl_alert_queue addObject:self];
    }
    return self;
}

- (void)addButtonWithTitle:(NSString *)title
                alertStyle:(HLAlertBoxActionType)style
              buttonAction:(HLAlertBoxButtonAction)handler {
    if (alertControllerAvailable()) {
        __weak __typeof(self) weakself = self;
        [(UIAlertController *)alertView addAction:
         [UIAlertAction actionWithTitle:title
                                  style:[self translateAlertStyleWithCustomStyle:style]
                                handler:^(UIAlertAction *__nonnull action) {
                                    __weak __typeof(weakself) strongself = weakself;
                                    if (handler) {
                                        handler();
                                    }
                                    [strongself cleanUp];
                                }]];
    } else {
        NSInteger buttonIndex = 0;
        if (_alertStyle == HLAlertBoxStyleAlertView) {
            buttonIndex = [((UIAlertView *)alertView)addButtonWithTitle:title];
            if (style == HLAlertBoxActionTypeCancel) {
                ((UIAlertView *)alertView).cancelButtonIndex = buttonIndex;
            }
        } else {
            buttonIndex = [((UIActionSheet *)alertView)addButtonWithTitle:title];
            if (style == HLAlertBoxActionTypeCancel) {
                ((UIActionSheet *)alertView).cancelButtonIndex = buttonIndex;
            }
            if (style == HLAlertBoxActionTypeDestructive) {
                ((UIActionSheet *)alertView).destructiveButtonIndex = buttonIndex;
            }
        }
        if (handler != NULL) {
            [actionButtonCallBacks setObject:handler forKey:@(buttonIndex)];
        }
    }
}

- (void)addTextFieldWithConfigurationHandler:(void (^__nullable)(UITextField *textField))configurationHandler {
    if (_alertStyle != HLAlertBoxStyleAlertView) {
        return;
    }
    if (alertControllerAvailable()) {
        [(UIAlertController *)alertView addTextFieldWithConfigurationHandler:configurationHandler];
    } else {
        NSMutableArray *textFieldArray = [self.textFields mutableCopy];
        if (textFieldArray.count >= 2) {
            return;
        }
        UITextField *textField = [[UITextField alloc] init];
        [textFieldArray addObject:textField];
        self.textFieldArray = [textFieldArray copy];
        if (configurationHandler) {
            configurationHandler(textField);
        }
        if (self.textFieldArray.count < 2) {
            ((UIAlertView *)alertView).alertViewStyle = UIAlertViewStylePlainTextInput;
        } else {
        ((UIAlertView *)alertView).alertViewStyle = UIAlertViewStylePlainTextInput;
        }
    }
}

- (void)show {
    if (alertControllerAvailable()) {
        if ([delegate isKindOfClass:[UIViewController class]] && delegate.view) {
            if (delegate.presentedViewController) {
                [delegate.presentedViewController presentViewController:((UIAlertController *)alertView) animated:YES completion:nil];
            } else {
                [delegate presentViewController:((UIAlertController *)alertView) animated:YES completion:nil];
            }
        } else {
            [[self topViewController] presentViewController:((UIAlertController *)alertView) animated:YES completion:^{
                
            }];
        }
    } else {
        if (_alertStyle == HLAlertBoxStyleAlertView) {
            [(UIAlertView *)alertView show];
        } else {
            if ([delegate isKindOfClass:[UIViewController class]] && delegate.view) {
                if (delegate.presentedViewController) {
                    [(UIActionSheet *)alertView showInView:delegate.presentedViewController.view];
                } else {
                    [(UIActionSheet *)alertView showInView:delegate.view];
                }
            } else {
                [(UIActionSheet *)alertView showInView:[self topViewController].view];
            }
        }
    }
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    HLAlertBoxButtonAction buttonAction = actionButtonCallBacks[@(buttonIndex)];
    if (buttonAction != NULL) {
        buttonAction();
    }
    
    [self performSelector:@selector(cleanUp) withObject:nil afterDelay:0];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    HLAlertBoxButtonAction buttonAction = actionButtonCallBacks[@(buttonIndex)];
    if (buttonAction != NULL) {
        buttonAction();
    }
    [self performSelector:@selector(cleanUp) withObject:nil afterDelay:0];
}

#pragma mark -
- (UIAlertActionStyle)translateAlertStyleWithCustomStyle:(HLAlertBoxActionType)style {
    switch (style) {
        case HLAlertBoxActionTypeDfault:
            return UIAlertActionStyleDefault;
        case HLAlertBoxActionTypeCancel:
            return UIAlertActionStyleCancel;
        case HLAlertBoxActionTypeDestructive:
            return UIAlertActionStyleDestructive;
    }
}
#pragma clang diagnostic pop

- (UIViewController *)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewControllerWithRootViewController:(UIViewController *)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabBarController = (UITabBarController *)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController *presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

#pragma mark - getter
- (NSArray *)textFieldArray {
    if (!_textFieldArray) {
        _textFieldArray = @[];
    }
    return _textFieldArray;
}

#pragma mark - depose
- (void)cleanUp {
    if ([hl_alert_queue containsObject:self]) {
        [hl_alert_queue removeObject:self];
    }
    
    if (0 == hl_alert_queue.count) {
        hl_alert_queue = nil;
    }
}

- (void)dealloc {
    
}
@end
