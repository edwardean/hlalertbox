//
//  ViewController.m
//  1
//
//  Created by LiHang on 15/7/6.
//  Copyright © 2015年 HangLee. All rights reserved.
//

#import "ViewController.h"
#import "HLAlertBox.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *alertButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [alertButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [alertButton setTitle:@"tap" forState:UIControlStateNormal];
    [alertButton addTarget:self action:@selector(showAlert) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:alertButton];
    
    [alertButton sizeToFit];
    alertButton.center = self.view.center;
}

- (void)showAlert {
    HLAlertBox *alertBox = [[HLAlertBox alloc] initWithDelegate:self style:HLAlertBoxStyleActionSheet title:@"ActionSheet" message:@"I'm a sheet"];
    [alertBox addButtonWithTitle:@"cacel" alertStyle:HLAlertBoxActionTypeCancel buttonAction:^{
        
    }];
    [alertBox addButtonWithTitle:@"OK" alertStyle:HLAlertBoxActionTypeDestructive buttonAction:^{
        
    }];
    [alertBox show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
