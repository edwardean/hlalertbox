//
//  AppDelegate.h
//  1
//
//  Created by LiHang on 15/7/6.
//  Copyright © 2015年 HangLee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

